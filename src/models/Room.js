export default class Room {
	constructor() {
		this.id = 0;
		this.name = '';
		this.image = '';
		this.price = 0;
		this.size = 0;
		this.bed = '';
		this.quantity = 0;
	}

		static fromJSON(json) {
		const self = new Room();

		// self.rooms = json.rooms.map(Room.fromJSON);
		// console.log(self.rooms);

		self.id = json.id;
		self.name = json.name;
		self.image = json.image;
		self.price = json.price;
		self.size = json.size;
		self.bed = json.bed;
		self.quantity = json.quantity;

		return self;
		console.log(this.self);
	}
}